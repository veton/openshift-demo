﻿using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.AspNetCore.Mvc;

namespace OpenShift.Demo.Cpu.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CpuController : ControllerBase
    {
        private static readonly TimeSpan LoadTime = TimeSpan.FromSeconds(1);

        [HttpGet("health")]
        public string Get() => "ok";

        [HttpGet("{id}")]
        public object Get(int id)
        {
            var stopwatch = Stopwatch.StartNew();
            while (stopwatch.Elapsed < LoadTime)
            {
                Thread.SpinWait(1_000_000);
            }

            return new
            {
                time = stopwatch.ElapsedMilliseconds
            };
        }
    }
}
