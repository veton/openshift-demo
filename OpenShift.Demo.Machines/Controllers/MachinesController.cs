﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace OpenShift.Demo.Machines.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MachinesController : Controller
    {
        private readonly IOptions<ServicesConfig> _servicesConfig;

        public MachinesController(IOptions<ServicesConfig> servicesConfig)
        {
            _servicesConfig = servicesConfig;
        }

        [HttpGet("health")]
        public string Get() => "ok";

        [HttpGet("{id}")]
        public async Task<object> Get(int id)
        {
            Task<object> cpuTask = GetResponse(_servicesConfig.Value.Cpu, id);
            Task<object> memoryTask = GetResponse(_servicesConfig.Value.Memory, id);
            return new
            {
                cpu = await cpuTask,
                memory = await memoryTask
            };
        }

        private static async Task<object> GetResponse(Uri baseUri, int id)
        {
            try
            {
                var uri = new Uri(baseUri, $"{id}");
                using (var client = new MyWebClient())
                {
                    var json = await client.DownloadStringTaskAsync(uri);
                    return JsonConvert.DeserializeObject(json);
                }
            }
            catch
            {
                return null;
            }
        }

        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                var w = base.GetWebRequest(uri);
                w.Timeout = 2000;
                return w;
            }
        }
    }
}
