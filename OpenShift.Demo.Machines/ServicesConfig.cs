﻿using System;

namespace OpenShift.Demo.Machines
{
    public class ServicesConfig
    {
        public Uri Cpu { get; set; }
        public Uri Memory { get; set; }
    }
}