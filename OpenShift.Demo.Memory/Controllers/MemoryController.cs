﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Mvc;

namespace OpenShift.Demo.Memory.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MemoryController : ControllerBase
    {
        private const int MemorySize = 32 * 1024 * 1204;
        private static readonly TimeSpan LoadTime = TimeSpan.FromSeconds(1);

        [HttpGet("health")]
        public string Get() => "ok";

        [HttpGet("{id}")]
        public object Get(int id)
        {
            const int blockSize = 32 * 1024; // Create by blocks to avoid LOH
            List<byte[]> buffer = Enumerable.Repeat(0, MemorySize / blockSize)
                .Select(i => new byte[blockSize])
                .ToList();
            Thread.Sleep(LoadTime);
            return new
            {
                size = buffer.Sum(b => b.Length)
            };
        }
    }
}
